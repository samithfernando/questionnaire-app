import { Directive, AfterViewInit, ElementRef } from "@angular/core";

@Directive({
  selector: '[appElementFocus]'
})
export class ElementFocusDirective implements AfterViewInit {

  constructor(private element: ElementRef) {}

  ngAfterViewInit() {
    this.element.nativeElement.focus();
  }

}
