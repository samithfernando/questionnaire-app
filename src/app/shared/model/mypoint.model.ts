import {CategoryEnum} from "./category.enum";
export class MyPoint {

  private name: CategoryEnum;
  avg?: any = 0;
  private count: number = 0;
  private total: number = 0;

  constructor(name: CategoryEnum) {
    this.name = name;
  }

  private feedAnswers(answer1: number, answer2: number): void {
    this.count++;
    this.total += (answer1 + answer2) / 2;
    this.avg = this.total / this.count;
  }

  public feedData(data: any): void {

    switch (this.name) {
      case CategoryEnum.Vision:
        this.feedAnswers(data.ans1, data.ans2);
        break;

      case CategoryEnum.Culture:
        this.feedAnswers(data.ans3, data.ans4);
        break;

      case CategoryEnum.Platform:
        this.feedAnswers(data.ans5, data.ans6);
        break;

      case CategoryEnum.Process:
        this.feedAnswers(data.ans7, data.ans8);
        break;

      case CategoryEnum.Ecosystem:
        this.feedAnswers(data.ans9, data.ans10);
        break;
    }
  }
}
