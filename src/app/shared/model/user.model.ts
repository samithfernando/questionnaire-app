import { DocumentReference } from "@firebase/firestore-types";

export class User {
    uid: string;
    email: string;
    photoUrl?: string;
    displayName?: string;
}