import { DocumentReference } from "@firebase/firestore-types";
import {MyPoint} from "./mypoint.model";

export class Answers {
    class_code:string;
    user:DocumentReference;
    profile:Profile;
    questionnaire:Questionnaire
}

class Profile{
    firmName:string;
    ans1:number;
    ans2:number;
    ans3:number;
    ans4:number;
}

class Questionnaire{
    ans1:number;
    ans2:number;
    ans3:number;
    ans4:number;
    ans5:number;
    ans6:number;
    ans7:number;
    ans8:number;
    ans9:number;
    ans10:number;
}

export class AnswerFeed{
  class_code:string;
  profile:Profile;
  respond:MyPoint[];
}
