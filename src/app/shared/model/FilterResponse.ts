import {CategoryEnum} from "./category.enum";
import {MyPoint} from "./mypoint.model";
/**
 * Created by Rasela on 5/16/2018.
 */

export class FilterResponse {

  private _categoriesData: MyPoint[] = [new MyPoint(CategoryEnum.Vision), new MyPoint(CategoryEnum.Culture), new MyPoint(CategoryEnum.Platform),
    new MyPoint(CategoryEnum.Process), new MyPoint(CategoryEnum.Ecosystem)];

  public feedData(data: any) {
    this._categoriesData.forEach(point => point.feedData(data));
  }

  public categoriesData(): MyPoint[] {
    return this._categoriesData;
  }
}
