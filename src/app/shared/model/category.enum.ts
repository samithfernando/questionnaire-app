/**
 * Created by Rasela on 5/16/2018.
 */

export enum CategoryEnum {

  Vision = "Vision",
  Culture = "Culture",
  Platform = "Platform",
  Process = "Process",
  Ecosystem = "Ecosystem"

}
