import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {Payload} from './payload.model'

export class ApiService {

    geoFilter: BehaviorSubject<number | null>;
    sectorFilter: BehaviorSubject<number | null>;
    revenueFilter: BehaviorSubject<number | null>;
    employeesFilter: BehaviorSubject<number | null>;
    userFilter: BehaviorSubject<any | null>;
    classFilter: BehaviorSubject<string | null>;

        constructor(payload:Payload) {
            this.geoFilter = new BehaviorSubject(payload.geo);
            this.sectorFilter = new BehaviorSubject(payload.sector);
            this.revenueFilter = new BehaviorSubject(payload.revenue);
            this.employeesFilter = new BehaviorSubject(payload.employerCount);
            this.userFilter = new BehaviorSubject(payload.user);
            this.classFilter = new BehaviorSubject(payload.classCode);
        }
}