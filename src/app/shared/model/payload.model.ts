
export class Payload {
    geo: number;
    sector:number;
    revenue: number;
    employerCount: number;
    user:any;
    classCode:string;
}

