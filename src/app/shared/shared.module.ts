import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {ElementFocusDirective} from "./directives/element-focus.directive";

@NgModule({
  imports: [ CommonModule, FormsModule, ReactiveFormsModule ],
  exports: [ CommonModule, FormsModule, ReactiveFormsModule, ElementFocusDirective ],
  declarations: [ElementFocusDirective]
})
export class SharedModule { }
