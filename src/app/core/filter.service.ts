import { Injectable } from "@angular/core";
import { DatabaseService } from "./database.service";
import { Observable } from "rxjs/Observable";

@Injectable()
export class FilterService {

  constructor(private db: DatabaseService) {}

  filterResults(payload: any): Observable<any> {
    return this.db.filter(payload);
  }

}
