import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {

  constructor() { }

  saveInSession(key: string, value: string) {
    sessionStorage.setItem(key, value);
  }

  getFromSession(key: string): string {
    return sessionStorage.getItem(key);
  }

  removeFromSession(key: string) {
    sessionStorage.removeItem(key);
  }

  clear() {
    sessionStorage.clear();
  }

}
