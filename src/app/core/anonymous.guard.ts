import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

@Injectable()
export class AnonymousGuard implements CanActivate {

  constructor(private auth: AuthService, private router: Router) {}


  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    return this.checkLoggedIn();

  }

  checkLoggedIn(): Observable<boolean> {
    return this.auth.authState
      .take(1)
      .map(user => !(!!user))
      .do(loggedIn => {
        if (!loggedIn) {
          console.log('access denied');
          this.auth.routeAllowed.next(false);
          this.router.navigate(['/home']);
        }
      })
  }
}
