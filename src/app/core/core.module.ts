import { NgModule } from '@angular/core';

import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AuthModule } from '../auth/auth.module';
import { AuthService } from './auth.service';
import { FilterService } from './filter.service';
import { DatabaseService } from './database.service'
import { AuthGuard } from './auth.guard';
import { StorageService } from "./storage.service";
import { AnonymousGuard } from "./anonymous.guard";
import { ComponentNavigationGuard } from "./component-navigation.guard";

@NgModule({
  imports: [
    AuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule
  ],
  providers: [
    AuthService,
    FilterService,
    DatabaseService,
    AuthGuard,
    StorageService,
    AnonymousGuard,
    ComponentNavigationGuard
  ]
})
export class CoreModule { }
