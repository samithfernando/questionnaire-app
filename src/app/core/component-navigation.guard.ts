import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, Router } from "@angular/router";
import { StorageService } from "./storage.service";

@Injectable()
export class ComponentNavigationGuard implements CanActivate {

  constructor(private storage: StorageService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot) {

    const title = route.data['title'];

    if (title === 'Questionnaire') {
      if (this.storage.getFromSession('DATA_PROFILE')) {
        return true;
      }
      this.router.navigate(['/user/profile/section1']);
      return false;
    }

    else if (title === 'Dashboard') {
      if (this.storage.getFromSession('DATA_PROFILE') && this.storage.getFromSession('DATA_QUES')) {
        return true;
      }
      this.router.navigate(['/user/profile/section1']);
      return false;
    }

  }

}
