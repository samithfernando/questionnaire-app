import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Router } from '@angular/router';
import { User } from '../auth/user.model';

import { Md5 } from 'ts-md5/dist/md5';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import {StorageService} from "./storage.service";



@Injectable()
export class AuthService {

  private routingAllowed = new BehaviorSubject(false);
  redirectUrl: string;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
    private storage: StorageService
  ) {}

  get authenticated() {
     return this.isVerifiedUser();
  }

  get currentUserId() {
    return this.authenticated ? this.afAuth.auth.currentUser.uid : null;
  }

  get authApp() {
    return this.afAuth.auth;
  }

  get authState() {
    return this.afAuth.authState;
  }

  emailSignIn(email: string, password: string) {
    return this.afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then(user => user);
  }

  emailSignUp(userData: any) {
    return this.afAuth.auth
      .createUserWithEmailAndPassword(userData.email, userData.password)
      .then(user => this.updateUserData(user, userData));
      //.then(() => this.afAuth.auth.currentUser.sendEmailVerification());
  }

  resetPassword(email: string) {
    return this.afAuth.auth.sendPasswordResetEmail(email)
  }

  private updateUserData(user, userData: any) {
    const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${user.uid}`);

    const data: User = {
      uid: user.uid,
      email: user.email || null,
      displayName: userData.firstName || null,
      lastName: userData.lastName || null,
      photoUrl: user.photoURL || `http://www.gravatar.com/avatar/${Md5.hashStr(user.uid)}/?d=identicon`
    };
    return userRef.set(data, { merge: true });
  }

  signOut() {
    return this.afAuth.auth.signOut()
      .then(() => {
      this.storage.clear();
      this.router.navigate['/']
    });
  }

  isVerifiedUser() {
    return this.afAuth.auth.currentUser
  }

  get routeAllowed() {
    return this.routingAllowed;
  }

}
