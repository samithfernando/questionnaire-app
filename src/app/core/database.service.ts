import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreDocument} from 'angularfire2/firestore';
import {Observable} from 'rxjs/Observable';
import {DocumentReference, CollectionReference, Query} from '@firebase/firestore-types';
import {Country} from '../shared/model/country.model';
import {EmployeesCount} from '../shared/model/employees.count.model';
import {Sectores} from '../shared/model/sectors.model';
import {AnnualRevenue} from '../shared/model/annual.revenue.model';
import {Question} from '../shared/model/question.model';
import {Answers, AnswerFeed} from '../shared/model/answers.model';
import {User} from '../shared/model/user.model';
import {Category} from '../shared/model/category.model'
import {MyPoint} from '../shared/model/mypoint.model';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {ApiService} from '../shared/model/api.service.model';
import {Payload} from '../shared/model/payload.model'
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/combineLatest';
import * as firebase from 'firebase';
import 'rxjs/add/observable/forkJoin';
import {CategoryEnum} from "../shared/model/category.enum";
import {FilterResponse} from "../shared/model/FilterResponse";
import * as moment from 'moment';

@Injectable()
export class DatabaseService {

  private countryPath = 'countries';
  private sectorPath = 'sectors';
  private employeeCountPath = 'employees';
  private annualRevenuePath = 'annual_revenue';
  private questionPath = 'questionnaire';
  private answersPath = 'answers';
  private categoryPath = 'categories';
  private questions: Observable<Question[]>;
  private categories: Observable<Category[]>;
  private answer: Observable<Answers[]>;
  private apiService: ApiService;
  private data;

  constructor(private db: AngularFirestore) {
  }

  getCountries(): Observable<any[]> {
    return this.db.collection<any>(this.countryPath, ref => ref.orderBy("id")).snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        data.id = +a.payload.doc.id;
        return data;
      });
    });

    //return this.db.collection<Country>(this.countryPath,ref => ref.orderBy('name', 'asc')).valueChanges();
  }

  getSectores(): Observable<any[]> {
    return this.db.collection<any>(this.sectorPath, ref => ref.orderBy("id")).snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        data.id = +a.payload.doc.id;
        return data;
      });
    });
  }

  getAnnualRevenue(): Observable<any[]> {
    return this.db.collection<any>(this.annualRevenuePath, ref => ref.orderBy("id")).snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        data.id = +a.payload.doc.id;
        return data;
      });
    });
  }

  getEmployeesCount(): Observable<any[]> {
    return this.db.collection<any>(this.employeeCountPath, ref => ref.orderBy("id")).snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        data.id = +a.payload.doc.id;
        return data;
      });
    });
  }

  getQuestions(): Observable<any[]> {
    return this.db.collection<any>(this.questionPath).snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        data.id = +a.payload.doc.id;
        return data;
      });
    });
  }

  saveCountry(country: Country): Promise<DocumentReference> {
    return this.db.collection<Country>(this.countryPath).add(country);
  }

  saveAnswer(answer: any): Promise<DocumentReference> {
    answer.date = firebase.firestore.FieldValue.serverTimestamp();
    answer.class_code = this.retrieveClassCode();
    return this.db.collection<any>(this.answersPath).add(answer);
  }

  getAnswers(): Observable<Answers[]> {
    return this.db.collection<Answers>(this.answersPath).valueChanges();
  }

  getUser(key: string): AngularFirestoreDocument<User> {
    return this.db.doc('users/' + key);
  }

  getCategories(): Observable<any[]> {
    return this.db.collection<any>(this.categoryPath, ref => ref.orderBy("id")).snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        data.id = +a.payload.doc.id;
        return data;
      });
    });
  }

  filter(payload: Payload): Observable<MyPoint[]> {

    let apiService = new ApiService(payload);
    return Observable.combineLatest(
      apiService.geoFilter,
      apiService.sectorFilter,
      apiService.revenueFilter,
      apiService.employeesFilter,
      apiService.userFilter,
      apiService.classFilter
    ).switchMap(([geo, sector, revenue, employees, user, classcode]) => {
        return this.db.collection(this.answersPath, ref => {
          let query: CollectionReference | Query = ref;
          if (user) {
            query = query.where('user', '==', user.ref)
          }
          else {
            if (geo) {
              query = query.where('profile.ans1', '==', geo)
            }
            if (sector) {
              query = query.where('profile.ans2', '==', sector)
            }
            if (revenue) {
              query = query.where('profile.ans3', '==', revenue)
            }
            if (employees) {
              query = query.where('profile.ans4', '==', employees)
            }
          }
          if (classcode) {
            query = query.where('class_code', '==', classcode)
          }
          return query;
        }).snapshotChanges().map(actions => {

          let filterResponse = new FilterResponse();

          actions.map(a => {
            const data = a.payload.doc.data().questionnaire;
            filterResponse.feedData(data);
          });

          return filterResponse.categoriesData();
        });
      }
    );
  }

  filter2(payload: Payload): Observable<AnswerFeed[]> {

    let apiService = new ApiService(payload);
    return Observable.combineLatest(
      apiService.geoFilter,
      apiService.sectorFilter,
      apiService.revenueFilter,
      apiService.employeesFilter,
      apiService.userFilter,
      apiService.classFilter
    ).switchMap(([geo, sector, revenue, employees, user, classcode]) => {
        return this.db.collection(this.answersPath, ref => {
          let query: CollectionReference | Query = ref;
          if (user) {
            query = query.where('user', '==', user.ref)
          }
          else {
            if (geo) {
              query = query.where('profile.ans1', '==', geo)
            }
            if (sector) {
              query = query.where('profile.ans2', '==', sector)
            }
            if (revenue) {
              query = query.where('profile.ans3', '==', revenue)
            }
            if (employees) {
              query = query.where('profile.ans4', '==', employees)
            }
          }
          if (classcode) {
            query = query.where('class_code', '==', classcode)
          }
          return query;
        }).snapshotChanges().map(actions => {
          return actions.map(a => {
            let answerFeed : AnswerFeed = new AnswerFeed();

            let filterResponse = new FilterResponse();
            let data = a.payload.doc.data().questionnaire;
            filterResponse.feedData(data);

            answerFeed.respond = filterResponse.categoriesData();
            answerFeed.class_code = a.payload.doc.data().class_code;
            answerFeed.profile = a.payload.doc.data().profile;

            return answerFeed;
          });
        });
      }
    );
  }

  getAUserAnswer(key: string): Observable<Answers[]> {
    const user = this.getUser(key);
    return this.db.collection<Answers>(this.answersPath,
      ref => {
        ref.where('user', '==', user.ref)
        return ref;
      })
      .valueChanges();
  }

  isAlreadySubmited(uid: string): Promise<any> {
    const user = this.getUser(uid);
    var collectionRef = this.db.collection<any>(this.answersPath);
    var queryRef = collectionRef.ref.where('user', '==', user.ref).where('class_code', '==', this.retrieveClassCode());
    return queryRef.get().then(querySnapshot => {
      var data = querySnapshot.docs.map(function (documentSnapshot) {
        let data = {
          "docId": documentSnapshot.ref.path,
          "data": documentSnapshot.data()
        }
        return data;
      });
      return data;
    }).catch(function (error) {
      console.log("Error getting document:", error);
    });
  }

  updateAnswer(docId: string, answer: any): Promise<any> {
    let documentRef = this.db.doc(docId);
    let sharedObject = answer;
    sharedObject.date = firebase.firestore.FieldValue.serverTimestamp();
    sharedObject.class_code = this.retrieveClassCode();
    return documentRef.update(sharedObject).then(value => {
      return true;
    });
  }

  retrieveClassCode(): string {
    //let date = new Date();
    //let dateStr: string = date.toLocaleDateString()
    let dateStr = moment().startOf('day').valueOf();
    return "" + dateStr;
  }

}
