import {Component, OnDestroy} from '@angular/core';
import {Event, NavigationEnd, NavigationError, NavigationStart, Router} from '@angular/router';
import {AuthService} from "./core/auth.service";
import {StorageService} from "./core/storage.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {

  user = null;
  isLoading = false;
  hasServerError = false;

   constructor(
     private router: Router,
     private auth: AuthService,
     private storage: StorageService
   ) {

     this.auth.routeAllowed.subscribe(allowed => {
       if (!allowed) {
         this.isLoading = false;
       }
     });

     router.events.subscribe( (event: Event) => {

       if (event instanceof NavigationStart) {
         // Show loading indicator
         this.isLoading = true;
       }

       if (event instanceof NavigationEnd) {
         // Hide loading indicator
         this.isLoading = false;
         window.scrollTo(0, 0);
       }

       if (event instanceof NavigationError) {
         // Hide loading indicator
         // Present error to user
         this.isLoading = false;
         this.hasServerError = true;
         alert('Server error detected');
         console.log(event.error);
       }
     });
   }

   ngOnDestroy () {
     this.storage.clear();
   }
}
