import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { NotFoundComponent } from './not-found.component';
import { FooterModule } from './footer/footer.module';
import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment';
import { TestComponent } from './test/test.component';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { LoadingComponent } from './loading.component';
import { ServerErrorComponent } from './server-error.component';
import { HomeComponent } from './home.component';
import { HeaderModule } from './header/header.module';


@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    TestComponent,
    LoadingComponent,
    ServerErrorComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    CoreModule,
    HeaderModule,
    FooterModule,
    AppRoutingModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
