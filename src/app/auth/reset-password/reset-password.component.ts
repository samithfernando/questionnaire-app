import { Component } from '@angular/core';
import { AuthService } from "../../core/auth.service";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent {

  isLoading: boolean = false;
  emailSent: boolean = false;
  email: string;
  notification: string;

  constructor(private auth: AuthService) {}

  sendPasswordResetLink() {

    if (!this.email) {
      return;
    }
    this.resetStates();
    this.isLoading = true;

    this.auth.resetPassword(this.email)
      .then(() => {
        // no error, email has been sent
        this.isLoading = false;
        this.emailSent = true;
      })
      .catch((error) => {
        console.error('reset error', error);

        if (error.code === 'auth/user-not-found') {
          this.notification = 'We could not find your email address';
          this.isLoading = false;
        }
        else if (error.code === 'auth/invalid-email') {
          this.notification = 'This email address is incorrect';
          this.isLoading = false;
        }
        else {
          this.notification = error.code;
          this.isLoading = false;
        }

      });
  }

  resetStates() {
    this.notification = null;
    this.emailSent = false;
  }

}
