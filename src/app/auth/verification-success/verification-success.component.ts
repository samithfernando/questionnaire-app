import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../core/auth.service';

@Component({
  selector: 'app-verification-success',
  templateUrl: './verification-success.component.html',
  styleUrls: ['./verification-success.component.scss']
})
export class VerificationSuccessComponent implements OnInit {

  mode: string;
  apiKey: string;
  oobCode: string;

  constructor(private route: ActivatedRoute, private auth: AuthService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.mode = params['mode'];
      this.apiKey = params['apiKey'];
      this.oobCode = params['oobCode'];

      this.handleVerifyEmail(this.auth.authApp, this.oobCode);
    });
  }

  handleVerifyEmail(auth, actionCode) {
    // Try to apply the email verification code.
    auth.applyActionCode(actionCode).then(function(resp) {
     console.log('resp email', resp);
    }).catch(function(error) {
      console.log(error);
    });
  }

}
