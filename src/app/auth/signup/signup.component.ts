import { Component } from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormControl, AbstractControl} from "@angular/forms";
import { AuthService } from "../../core/auth.service";
import { Router } from "@angular/router";
import { passwordMatch } from './password-match';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss', '../auth.style.scss']
})
export class SignupComponent {

  signUpForm: FormGroup;
  firstNameCtrl: FormControl;
  lastNameCtrl: FormControl;
  emailCtrl: FormControl;
  passwordCtrl: FormControl;
  confirmPwdCtrl: FormControl;
  loading = false;
  togglePassword = false;

  emailAlreadyInUsed = false;

  constructor(
    public fb: FormBuilder,
    public auth: AuthService,
    private router: Router
  ) {

    this.firstNameCtrl = fb.control('', Validators.required );
    this.lastNameCtrl = fb.control('', Validators.required );
    this.emailCtrl = fb.control('', [ Validators.required, Validators.email ]);
    this.passwordCtrl = fb.control('', [
        Validators.required,
        Validators.minLength(6)
    ]);
    this.confirmPwdCtrl = fb.control('', Validators.required);

    this.signUpForm = this.fb.group({
      firstName: this.firstNameCtrl,
      lastName: this.lastNameCtrl,
      email: this.emailCtrl,
      password: this.passwordCtrl,
      confirmPassword: this.confirmPwdCtrl
    }, { validator: passwordMatch });
  }

  get firstName() {
    return this.signUpForm.get('firstName');
  }

  get lastName() {
    return this.signUpForm.get('lastName');
  }

  get email() {
    return this.signUpForm.get('email');
  }

  get password() {
    return this.signUpForm.get('password');
  }

  signUp() {
    this.loading = true;
    this.emailAlreadyInUsed = false;

    const userData: any = {
      email: this.email.value,
      password: this.password.value,
      firstName: this.firstName.value,
      lastName: this.lastName.value
    }

    return this.auth.emailSignUp(userData)
      .then(user => {
          console.log('My USER', user);
          this.router.navigate(['/auth/verification']);
      })
      .catch(error => {
        console.error(error);
        if (error.code === 'auth/email-already-in-use') {
          this.emailAlreadyInUsed = true;
        }
        this.loading = false;
      });
  }

}
