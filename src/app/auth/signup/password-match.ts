import { AbstractControl } from '@angular/forms';

export function passwordMatch (control: AbstractControl): {[key: string]: boolean}  {

  const pwd = control.get('password');
  const confirmPwd = control.get('confirmPassword');

  if (!pwd || !confirmPwd) return null;

  if (pwd.value === confirmPwd.value) {
    return null;
  }
  return { mismatch: true };
}
