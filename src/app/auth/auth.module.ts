import { NgModule } from '@angular/core';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from "@angular/router";
import { EmailVerificationComponent } from './email-verification/email-verification.component';
import { VerificationSuccessComponent } from './verification-success/verification-success.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { AnonymousGuard } from "../core/anonymous.guard";

const AUTH_ROUTES = [
  {
    path: 'login',
    canActivate: [AnonymousGuard],
    component: SigninComponent
  },
  {
    path: 'signup',
    canActivate: [AnonymousGuard],
    component: SignupComponent
  },
  {
    path: 'reset-password',
    canActivate: [AnonymousGuard],
    component: ResetPasswordComponent
  },
  {
    path: 'auth/verification',
    component: EmailVerificationComponent
  },
  {
    path: 'auth/verified',
    component: VerificationSuccessComponent
  },
]

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(AUTH_ROUTES)
  ],
  declarations: [
    SigninComponent,
    SignupComponent,
    EmailVerificationComponent,
    VerificationSuccessComponent,
    ResetPasswordComponent
  ]
})
export class AuthModule { }
