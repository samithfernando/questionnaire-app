export interface User {
  uid: string;
  email: string;
  photoUrl?: string;
  displayName?: string;
  lastName?: string;
}
