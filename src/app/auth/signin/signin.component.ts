import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../../core/auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss', '../auth.style.scss']
})
export class SigninComponent {

  signInForm: FormGroup;
  emailCtrl: FormControl;
  passwordCtrl: FormControl;
  loading = false;
  togglePassword = false;

  invalidUnOrPw = false;
  emailNotVerified = false;
  noUserWithEmail = false;

  constructor(
    public fb: FormBuilder,
    public auth: AuthService,
    private router: Router
  ) {
    this.emailCtrl = fb.control('', [ Validators.required, Validators.email ]);
    this.passwordCtrl = fb.control('', Validators.required);

    this.signInForm = this.fb.group({
      email: this.emailCtrl,
      password: this.passwordCtrl
    });
  }

  get email() {
    return this.signInForm.get('email');
  }

  get password() {
    return this.signInForm.get('password');
  }

  signIn() {
    this.loading = true;
    this.resetFieldState();

    return this.auth.emailSignIn(this.email.value, this.password.value)
      .then(user => {
        if (user) {
          if (this.auth.redirectUrl) {
            this.router.navigateByUrl(this.auth.redirectUrl);
          } else {
            this.router.navigate(['/home']);
          }
          /*if (user.emailVerified) {
            if (this.auth.redirectUrl) {
                this.router.navigateByUrl(this.auth.redirectUrl);
            } else {
              this.router.navigate(['/home']);
            }
          } else {
            this.emailNotVerified = true;
            this.loading = false;
          }*/
        } else {
          this.invalidUnOrPw = true;
          this.loading = false;
        }
      })
      .catch(error => {
        console.error('logging error', error);

        if (error.code === 'auth/user-not-found') {
          this.noUserWithEmail = true;
        } else {
          this.invalidUnOrPw = true;
        }
        this.loading = false;
      });
  }

  resetFieldState() {
    this.invalidUnOrPw = false;
    this.noUserWithEmail = false;
  }



}
