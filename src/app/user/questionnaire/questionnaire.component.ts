import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Question } from '../../shared/model/question.model';
import { StorageService } from '../../core/storage.service';
import { Answers } from '../../shared/model/answers.model';
import { AuthService } from '../../core/auth.service';
import { DatabaseService } from '../../core/database.service';

@Component({
  selector: 'app-questionnaire',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.scss']
})
export class QuestionnaireComponent implements OnInit {

  title: string;
  questionList: Question[];
  user: any;

  loopCount = [1,2,3,4,5];

  questionnaireAnswers = {
    ans1: null,
    ans2: null,
    ans3: null,
    ans4: null,
    ans5: null,
    ans6: null,
    ans7: null,
    ans8: null,
    ans9: null,
    ans10: null
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private storage: StorageService,
    private auth: AuthService,
    private db: DatabaseService
  ) { }

  ngOnInit() {

    this.title = this.route.snapshot.data['title'];
    this.questionList = this.route.snapshot.data['questions'];

    // get submitted answers from session if any
    const questionnaireAnswers = this.storage.getFromSession('DATA_QUES');
    if (questionnaireAnswers) {
      // assign answers to the existing model
      this.questionnaireAnswers = JSON.parse(questionnaireAnswers);
    }

  }

  goToAnalyze() {
    this.saveToSession();

    const profileAnswers = JSON.parse(this.storage.getFromSession('DATA_PROFILE'));
    const questionnaireAnswers = JSON.parse(this.storage.getFromSession('DATA_QUES'));

    const answers: Answers = {
      class_code: null,
      user: this.db.getUser(this.auth.currentUserId).ref,
      profile: profileAnswers,
      questionnaire: questionnaireAnswers
    }

    // update request
    if (this.storage.getFromSession('DOC_ID')) {
      this.db.updateAnswer(this.storage.getFromSession('DOC_ID'), answers).then(resp => {
        this.router.navigate(['dashboard'], { relativeTo: this.route.parent });
        return;
      }).catch(error => {
        console.log('error', error);
      });
    }

    // create new request
    else {
      this.db.saveAnswer(answers).then(doc => {
        // console.log('my resp', doc);
        this.storage.removeFromSession('DOC_ID');
        this.storage.saveInSession('DOC_ID', 'answers/' + doc.id);
        this.router.navigate(['dashboard'], { relativeTo: this.route.parent });
        return;
      }).catch(error => {
        console.log('error', error);
      });
    }
  }

  goToSection1() {
    this.saveToSession();
    this.router.navigate(['section1'], { relativeTo: this.route.parent });
  }

  saveToSession() {
    this.storage.removeFromSession('DATA_QUES');
    this.storage.saveInSession('DATA_QUES', JSON.stringify(this.questionnaireAnswers));
  }

  isDataValid() {
    return this.noNullableFields(this.questionnaireAnswers);
  }

  // return true if any field contains a null value
  noNullableFields(o) {
    return Object.keys(o).every((key) => {
      return o[key];
    });
  }

}
