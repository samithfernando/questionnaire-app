import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { DatabaseService } from '../core/database.service';
import { Observable } from 'rxjs/Observable';
import { Country } from '../shared/model/country.model';

import 'rxjs/add/operator/first';

@Injectable()
export class CountryListResolver implements Resolve<Country[]> {

  constructor(private db: DatabaseService) { }

  resolve(): Observable<Country[]> {
    return this.db.getCountries().first();
  }

}
