import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { DatabaseService } from '../core/database.service';
import { Question } from '../shared/model/question.model';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/first';

@Injectable()
export class QuestionsResolver implements Resolve<Question[]> {

  constructor(private db: DatabaseService) { }

  resolve(): Observable<Question[]> {
     return this.db.getQuestions().first();
  }

}
