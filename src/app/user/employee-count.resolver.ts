import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { DatabaseService } from '../core/database.service';
import { Observable } from 'rxjs/Observable';
import { EmployeesCount } from '../shared/model/employees.count.model';

import 'rxjs/add/operator/first';

@Injectable()
export class EmployeeCountResolver implements Resolve<EmployeesCount[]> {

  constructor(private db: DatabaseService) { }

  resolve(): Observable<EmployeesCount[]> {
    return this.db.getEmployeesCount().first();
  }

}
