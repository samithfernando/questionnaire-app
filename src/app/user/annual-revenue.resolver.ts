import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { DatabaseService } from '../core/database.service';
import { Observable } from 'rxjs/Observable';
import { AnnualRevenue } from '../shared/model/annual.revenue.model';

import 'rxjs/add/operator/first';

@Injectable()
export class AnnualRevenueResolver implements Resolve<AnnualRevenue[]> {

  constructor(private db: DatabaseService) { }

  resolve(): Observable<AnnualRevenue[]> {
    return this.db.getAnnualRevenue().first();
  }

}
