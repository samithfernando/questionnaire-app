import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { StorageService } from '../../core/storage.service';
import { DatabaseService } from '../../core/database.service';

import { AuthService } from '../../core/auth.service';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/first';

@Injectable()
export class ScoreResolve implements Resolve<any> {

  constructor(private storage: StorageService, private db: DatabaseService, private auth: AuthService) { }
  resolve() {
    const uid = this.auth.currentUserId;
    const classCode = this.storage.getFromSession('class-code');

    // 'my score' payload
    const payload = {
      geo: null,
      sector: null,
      revenue: null,
      employerCount: null,
      user: this.db.getUser(uid),
      classCode: classCode
    }

    // 'all score' payload
    const payload2 = {
      geo: null,
      sector: null,
      revenue: null,
      employerCount: null,
      user: null,
      classCode: null
    }

    return Observable.forkJoin(
      this.db.filter(payload).first(),
      this.db.filter(payload2).first()
    );
  }

}
