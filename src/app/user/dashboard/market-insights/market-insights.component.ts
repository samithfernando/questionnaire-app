import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import * as Chart from 'chart.js';
import { StorageService } from '../../../core/storage.service';
import { FilterService } from "../../../core/filter.service";

declare const $: any;
declare const html2canvas: any;

@Component({
  selector: 'app-market-insights',
  templateUrl: './market-insights.component.html',
  styleUrls: ['./market-insights.component.scss', '../dashboard.component.scss']
})
export class MarketInsightsComponent implements OnInit, AfterViewInit {

  graph2: any;

  @Input() myScore;
  @Input() allScore;
  @Input() geoList;
  @Input() sectorList;
  @Input() revenueList;
  @Input() employeeList;

  selectedGeoId = null;
  selectedSectorId = null;
  selectedRevenueId = null;
  selectedEmpCountId = null;

  classCode: string;

  chartViewActive = true;
  dataViewActive = false;

  myScoreAvg: number;
  allScoreAvg: number;

  constructor(
    private storage: StorageService,
    private filter: FilterService
  ) {}

  ngOnInit() {
    this.classCode = this.storage.getFromSession('class-code');
    this.myScoreAvg = this.getAverage(this.myScore);
    this.allScoreAvg = this.getAverage(this.allScore);
  }

  ngAfterViewInit() {
    const graph2Options = {
      type: 'radar',
      data: {
        labels: ['Vision', 'Culture', 'Platform', 'Process', 'Ecosystem'],
        datasets: [
          {
            data: this.myScore,
            borderColor: '#fc002c',
            label: 'My Score',
            fill: false,
            backgroundColor: '#fc002c',
            borderWidth: 2,
            pointBackgroundColor: '#fc002c',
          },
          {
            data: this.allScore,
            borderColor: '#46e6f2',
            label: 'Overall Score',
            fill: false,
            backgroundColor: '#46e6f2',
            borderWidth: 2,
            pointBackgroundColor: '#46e6f2',
          }
        ]
      },
      options: {
        legend: {
          display: false
        },
        elements: {
          point: {
            pointStyle: 'circle',
            radius: 4,
          }
        },
        scale: {
          pointLabels: {
            fontSize: 14,
            fontStyle: 'normal',
            fontFamily: 'Circular Std Book'
          },
          ticks: {
            min: 0,
            max: 5
          }
        }
      }
    };

    this.graph2 = new Chart(document.getElementById('allScore'), graph2Options);
  }

  payload: any = {
    user: null,
    classCode: null,
    geo: null,
    sector: null,
    revenue: null,
    employerCount: null
  }

  updateGraph(optionId: any, category: string) {

    optionId = isNaN(optionId) ? null : +optionId;

    switch(category) {
      case 'geo':
        this.payload['geo'] = optionId;
        break;
      case 'sector':
        this.payload['sector'] = optionId;
        break;
      case 'revenue':
        this.payload['revenue'] = optionId;
        break;
      case 'employee':
        this.payload['employerCount'] = optionId;
        break;
    }

    console.log('payload', this.payload);

    return this.filter.filterResults(this.payload).subscribe(scores => {
        console.log('ss', scores);
        const scoresAvgArray = scores.map(score => +score.avg.toFixed(1));
        this.allScoreAvg = this.getAverage(scoresAvgArray);

        this.allScore = scoresAvgArray; // used for data view

        this.graph2.data.datasets[1].data = scoresAvgArray;

        this.reDrawGraph();
      }
    );

    /*return this.db.filter(this.payload).subscribe(scores => {
      const scoresAvgArray = scores.map(score => +score.avg.toFixed(1));
      this.allScoreAvg = this.getAverage(scoresAvgArray);

      this.allScore = scoresAvgArray; // used for data view

      this.graph2.data.datasets[1].data = scoresAvgArray;

      this.reDrawGraph();
    });*/
  }

  showChartView() {
    this.chartViewActive = true;
    this.dataViewActive = false;
  }

  showDataView() {
    this.chartViewActive = false;
    this.dataViewActive = true;
  }

  reDrawGraph() {
    this.graph2.update({
      easing: 'easeInOutExpo',
      duration: 800
    });
  }

  printGraph(id) {
    html2canvas($(`#${id}`).get(0)).then((canvas) => {

      const dataUrl = canvas.toDataURL();

      let windowContent = '<!DOCTYPE html>';
      windowContent += '<html>';
      windowContent += '<head><title>Print canvas</title></head>';
      windowContent += '<body>';
      windowContent += '<img src="' + dataUrl + '">';
      windowContent += '</body>';
      windowContent += '</html>';

      const printWin = window.open('', '', 'width=' + screen.availWidth + ',height=' + screen.availHeight);
      printWin.document.open();
      printWin.document.write(windowContent);

      printWin.document.addEventListener('load', function () {
        printWin.focus();
        printWin.print();
        printWin.document.close();
        printWin.close();
      }, true);
    });
  }

  getAverage(arr: Array<number>): number {
    return +(arr.reduce( ( p, c ) => p + c, 0 ) / arr.length);
  }

  update(event) {
    console.log(event);
  }

}
