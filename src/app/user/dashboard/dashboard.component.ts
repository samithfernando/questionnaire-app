import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import 'rxjs/add/operator/first';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  title: string;
  scores: any [];
  geoList: any [];
  sectorList: any [];
  revenueList: any [];
  employeeList: any [];
  myScore;
  allScore;

  constructor(private route: ActivatedRoute) {
    this.title = this.route.snapshot.data['title'];
    this.scores = this.route.snapshot.data['scores'];
    this.geoList = this.route.snapshot.data['geoList'];
    this.sectorList = this.route.snapshot.data['sectorList'];
    this.revenueList = this.route.snapshot.data['revenueList'];
    this.employeeList = this.route.snapshot.data['employeeList'];
  }

  ngOnInit() {
    this.myScore = this.scores[0].map(score => +score.avg.toFixed(1));
    this.allScore = this.scores[1].map(score => +score.avg.toFixed(1));

    console.log('myScore', this.myScore);
    console.log('allScore', this.allScore);
  }

}
