import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import * as Chart from 'chart.js';

declare const $: any;
declare const html2canvas: any;

@Component({
  selector: 'app-compare-with-colleagues',
  templateUrl: './compare-with-colleagues.component.html',
  styleUrls: ['./compare-with-colleagues.component.scss', '../dashboard.component.scss']
})
export class CompareWithColleaguesComponent implements OnInit, AfterViewInit {

  groupScoreChart: any;

  @Input() myScore;
  @Input() allScore;

  myScoreAvg: number;
  allScoreAvg: number;

 /* allViewActive = false;
  classViewActive = true;*/

  chartViewActive = true;
  dataViewActive = false;

  constructor() { }

  ngOnInit() {
    this.myScoreAvg = this.getAverage(this.myScore);
    this.allScoreAvg = this.getAverage(this.allScore);
  }

  ngAfterViewInit() {
    const graph1 = {
      type: 'radar',
      data: {
        labels: ['Vision', 'Culture', 'Platform', 'Process', 'Ecosystem'],
        datasets: [
          {
            data: this.myScore,
            borderColor: '#fc002c',
            label: 'My Score',
            fill: false,
            backgroundColor: '#fc002c',
            borderWidth: 2,
            pointBackgroundColor: '#fc002c',
          },
          {
            data: this.allScore,
            borderColor: '#46e6f2',
            label: 'Overall Score',
            fill: false,
            backgroundColor: '#46e6f2',
            borderWidth: 2,
            pointBackgroundColor: '#46e6f2',
          }
        ]
      },
      options: {
        legend: {
          display: false
        },
        elements: {
          point: {
            pointStyle: 'circle',
            radius: 4,
          }
        },
        scale: {
          pointLabels: {
            fontSize: 14,
            fontStyle: 'normal',
            fontFamily: 'Circular Std Book'
          },
          ticks: {
            min: 0,
            max: 5
          }
        }
      }
    };

    this.groupScoreChart = new Chart(document.getElementById('groupScore'), graph1);
  }

  getAverage(arr: Array<number>): number {
    return +(arr.reduce( ( p, c ) => p + c, 0 ) / arr.length);
  }

  printGraph(id) {
      html2canvas($(`#${id}`).get(0)).then((canvas) => {

        const dataUrl = canvas.toDataURL();

        let windowContent = '<!DOCTYPE html>';
        windowContent += '<html>';
        windowContent += '<head><title>Print canvas</title></head>';
        windowContent += '<body>';
        windowContent += '<img src="' + dataUrl + '">';
        windowContent += '</body>';
        windowContent += '</html>';

        const printWin = window.open('', '', 'width=' + screen.availWidth + ',height=' + screen.availHeight);
        printWin.document.open();
        printWin.document.write(windowContent);

        printWin.document.addEventListener('load', function () {
          printWin.focus();
          printWin.print();
          printWin.document.close();
          printWin.close();
        }, true);
    });
  }

  sendEmail(id) {
    html2canvas($(`#${id}`).get(0)).then((canvas) => {

      const dataUrl = canvas.toDataURL();

      let windowContent = '<!DOCTYPE html>';
      windowContent += '<html>';
      windowContent += '<head><title>Print canvas</title></head>';
      windowContent += '<body>';
      windowContent += '<img src="' + dataUrl + '">';
      windowContent += '</body>';
      windowContent += '</html>';

      const mailBody = windowContent;
      window.open("mailto:yourmail@domain.com?subject=hii&body="+encodeURI(mailBody));
    });

  }

  showChartView() {
    this.chartViewActive = true;
    this.dataViewActive = false;
  }

  showDataView() {
    this.chartViewActive = false;
    this.dataViewActive = true;
  }

}
