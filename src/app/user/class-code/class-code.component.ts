import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { StorageService } from "../../core/storage.service";

@Component({
  selector: 'app-class-code',
  templateUrl: './class-code.component.html',
  styleUrls: ['./class-code.component.scss']
})
export class ClassCodeComponent implements OnInit {

  title: string;
  classCode: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private storage: StorageService,
  ) { }

  ngOnInit() {
    this.title = this.route.snapshot.data['title'];

    const classCode = this.storage.getFromSession('class-code');
    if (classCode) {
      this.classCode = classCode;
    }
  }

  goToSectionOne() {
    this.storage.saveInSession('class-code', this.classCode);
    this.router.navigate(['section1'], { relativeTo: this.route.parent });
  }

}
