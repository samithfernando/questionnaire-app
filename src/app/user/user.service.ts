import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore, AngularFirestoreDocument } from "angularfire2/firestore";
import { User } from "./user.model";

@Injectable()
export class UserService {

  userCollection: AngularFirestoreCollection<User>;
  userDoc: AngularFirestoreDocument<User>;

  constructor(private afs: AngularFirestore) { }

  getUsers() {
    this.userCollection = this.afs.collection<User>('users');
    return this.userCollection.valueChanges();
  }

  getUser(uid: string) {
    console.log('user service', uid);
    this.userDoc = this.afs.doc<User>(`users/${uid}`);
    return this.userDoc.valueChanges();
  }



}
