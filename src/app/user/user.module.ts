import { NgModule } from '@angular/core';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { HeaderModule } from '../header/header.module';
import { FooterModule } from '../footer/footer.module';
import { UserService } from './user.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GeographicalDataComponent } from './geographical-data/geographical-data.component';
import { ClassCodeComponent } from './class-code/class-code.component';
import { QuestionnaireComponent } from './questionnaire/questionnaire.component';
import { HeadingComponent } from './heading/heading.component';
import { QuestionsResolver } from './questions.resolver';
import { CountryListResolver } from './contry-list.resolver';
import { SectorsResolver } from './sectors.resolver';
import { EmployeeCountResolver } from './employee-count.resolver';
import { AnnualRevenueResolver } from './annual-revenue.resolver';
import { AuthGuard } from '../core/auth.guard';
import { CompareWithColleaguesComponent } from './dashboard/compare-with-colleagues/compare-with-colleagues.component';
import { MarketInsightsComponent } from './dashboard/market-insights/market-insights.component';
import { ScoreResolve } from './dashboard/score.resolve';
import { UserHasAnswerResolver } from "./user-has-answer.resolver";
import { ComponentNavigationGuard } from "../core/component-navigation.guard";

const user_routes: Routes = [
  { path: '', redirectTo: 'profile', pathMatch: 'full' },
  // { path: 'profile/:uid', component: UserProfileComponent},
  {
    path: 'profile',
    component: UserProfileComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'code', pathMatch: 'full' },
      // { path: 'code', component: ClassCodeComponent, data: {title: ''} },
      {
        path: 'section1',
        component: GeographicalDataComponent,
        data: { title: 'Profile' },
        resolve: {
          countryList: CountryListResolver,
          annualRevenue: AnnualRevenueResolver,
          employeeCount: EmployeeCountResolver,
          sectors: SectorsResolver,
          hasAnswers: UserHasAnswerResolver
        }
      },
      {
        path: 'section2',
        component: QuestionnaireComponent,
        data: { title: 'Questionnaire' },
        canActivate: [ComponentNavigationGuard],
        resolve: { questions: QuestionsResolver }
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        data: { title: 'Dashboard' },
        canActivate: [ComponentNavigationGuard],
        resolve: {
          scores: ScoreResolve,
          geoList: CountryListResolver,
          sectorList: SectorsResolver,
          revenueList: AnnualRevenueResolver,
          employeeList: EmployeeCountResolver
        }
      },
    ]
  }
]

@NgModule({
  imports: [
    SharedModule,
    HeaderModule,
    FooterModule,
    RouterModule.forChild(user_routes)
  ],
  declarations: [
    HeadingComponent,
    UserProfileComponent,
    DashboardComponent,
    GeographicalDataComponent,
    ClassCodeComponent,
    QuestionnaireComponent,
    CompareWithColleaguesComponent,
    MarketInsightsComponent
  ],
  providers: [
    UserService,
    QuestionsResolver,
    CountryListResolver,
    EmployeeCountResolver,
    SectorsResolver,
    AnnualRevenueResolver,
    ScoreResolve,
    UserHasAnswerResolver
  ]
})
export class UserModule { }
