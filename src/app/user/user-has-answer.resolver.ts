import {Injectable} from "@angular/core";
import {Resolve} from "@angular/router";
import {DatabaseService} from "../core/database.service";
import {AuthService} from "../core/auth.service";
import {StorageService} from "../core/storage.service";
import {Observable} from "rxjs/Observable";

@Injectable()
export class UserHasAnswerResolver implements Resolve<string> {

  constructor(
    private db: DatabaseService,
    private auth: AuthService
  ) {}

  resolve(): Observable<string> {
    return Observable.fromPromise(
      this.db.isAlreadySubmited(this.auth.currentUserId)
    )
  }


}
