import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { DatabaseService } from '../core/database.service';
import { Observable } from 'rxjs/Observable';
import { Sectores } from '../shared/model/sectors.model';

import 'rxjs/add/operator/first';

@Injectable()
export class SectorsResolver implements Resolve<Sectores[]> {

  constructor(private db: DatabaseService) { }

  resolve(): Observable<Sectores[]> {
    return this.db.getSectores().first();
  }

}
