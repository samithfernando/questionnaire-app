import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Country } from "../../shared/model/country.model";
import { Sectores } from "../../shared/model/sectors.model";
import { AnnualRevenue } from "../../shared/model/annual.revenue.model";
import { EmployeesCount } from "../../shared/model/employees.count.model";
import { StorageService } from "../../core/storage.service";

@Component({
  selector: 'app-geographical-data',
  templateUrl: './geographical-data.component.html',
  styleUrls: ['./geographical-data.component.scss']
})
export class GeographicalDataComponent implements OnInit {

  title: string;
  countryList: Country[];
  sectorList: Sectores[];
  revenueList: AnnualRevenue[];
  employeeCountList: EmployeesCount[];
  hasAnswers: any[];

  profileAnswers = {
    firmName: null,
    ans1: null,
    ans2: null,
    ans3: null,
    ans4: null
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private storage: StorageService
  ) {}

  ngOnInit() {

    // Get all resolvers data
    this.title = this.route.snapshot.data['title'];
    this.countryList = this.route.snapshot.data['countryList'];
    this.employeeCountList = this.route.snapshot.data['employeeCount'];
    this.sectorList = this.route.snapshot.data['sectors'];
    this.revenueList = this.route.snapshot.data['annualRevenue'];
    this.hasAnswers = this.route.snapshot.data['hasAnswers'];

    // get submitted answers from session if any
    const submittedProfileData: string = this.storage.getFromSession('DATA_PROFILE');

    // assign answers to the existing model
    if (submittedProfileData) {
      this.profileAnswers = JSON.parse(submittedProfileData);

    } else {
      // no data in session(cleared) or new user
      // check whether a user already has a previous submission
      if (this.hasAnswers[0]) {
        const { data: { profile, questionnaire }, docId } = this.hasAnswers[0];

        this.profileAnswers = profile;
        this.storage.removeFromSession('DOC_ID');
        this.storage.removeFromSession('DATA_PROFILE');
        this.storage.removeFromSession('DATA_QUES');
        this.storage.saveInSession('DOC_ID', docId);
        this.storage.saveInSession('DATA_PROFILE', JSON.stringify(profile));
        this.storage.saveInSession('DATA_QUES', JSON.stringify(questionnaire));
      }
    }
  }

  goToSectionTwo() {
    this.storage.saveInSession('DATA_PROFILE', JSON.stringify(this.profileAnswers))
    this.router.navigate(['section2'], { relativeTo: this.route.parent });
  }

  isDataValid() {
    if (this.profileAnswers.firmName && this.profileAnswers.ans1 &&
      this.profileAnswers.ans2 && this.profileAnswers.ans3 && this.profileAnswers.ans4) {
      return true;
    } else {
      return false;
    }
  }

}
