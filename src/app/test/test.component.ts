import { Component, OnInit } from '@angular/core';
import {AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection} from 'angularfire2/firestore';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { DocumentReference } from '@firebase/firestore-types';
import { DatabaseService } from '../core/database.service';
import { Country } from '../shared/model/country.model';
import { EmployeesCount } from '../shared/model/employees.count.model';
import { Sectores } from '../shared/model/sectors.model';
import { AnnualRevenue } from '../shared/model/annual.revenue.model';
import { Question } from '../shared/model/question.model';
import { Answers } from '../shared/model/answers.model';
import { User } from '../shared/model/user.model';
import {Category} from '../shared/model/category.model';
import { MyPoint } from '../shared/model/mypoint.model';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { Payload } from '../shared/model/payload.model';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  countries: Observable<any[]>;
  sectores: Observable<Sectores[]>;
  employeesCounts: Observable<EmployeesCount[]>;
  annualRevenues: Observable<AnnualRevenue[]>;
  questions: Observable<Question[]>;
  answers: Observable<Answers[]>;
  user: AngularFirestoreDocument<User>;
  answer: Answers;
  categories:Observable<any[]>;
  mypoit:Observable<MyPoint[]>;
  avg1:Observable<MyPoint[]>;
  avg2:Observable<MyPoint[]>;
  avg3:Observable<MyPoint[]>;
  test:any;

  geoFilter: BehaviorSubject<number | null>;
  sectorFilter: BehaviorSubject<number | null>;
  revenueFilter: BehaviorSubject<number | null>;
  employeesFilter: BehaviorSubject<number | null>;
  userFilter: BehaviorSubject<any | null>;
  classFilter: BehaviorSubject<number | null>;


  payload1:Payload;
  payload2:Payload;
  payload3:Payload;

  mydata;


  constructor(
    private db: AngularFirestore,
    private db2: AngularFireDatabase,
    private databaseService: DatabaseService
  ) {

  }

  ngOnInit() {
    this.runTest2();
  }

  answersCollection: AngularFirestoreCollection<any>;
  answersObservable$: Observable<any[]>;

  runTest2() {
    this.answersCollection = this.db.collection('answers');
    this.answersObservable$ = this.answersCollection.valueChanges();

    this.answersObservable$.subscribe(data => {
      console.log('data', data);
    });


  }

  runTests() {
    this.countries = this.databaseService.getCountries();
    this.sectores = this.databaseService.getSectores();
    this.employeesCounts = this.databaseService.getEmployeesCount();
    this.annualRevenues = this.databaseService.getAnnualRevenue();
    this.questions = this.databaseService.getQuestions();
    this.answers = this.databaseService.getAnswers();
    this.categories=this.databaseService.getCategories();

    //this.mypoit=this.databaseService.getAUserPoints('j1EaPBIsplZLK0cVGQK6J3yXa0c2');
    this.user = this.databaseService.getUser('lJeCkMUhbheFX2gHgtqt4QnqV7B3');


    this.payload1={
      geo:null,
      sector:null,
      revenue: null,
      employerCount:null,
      user:null,
      classCode: null
    }

    this.payload2={
      geo:null,
      sector:null,
      revenue: null,
      employerCount:null,
      user: null,
      classCode: 'Auxenta'
    }


    this.payload3={
      geo:null,
      sector:null,
      revenue: null,
      employerCount:null,
      user: null,
      classCode:null
    }

    this.avg1=this.databaseService.filter(this.payload1);
    //this.avg2=this.databaseService.filter(this.payload2);


    this.mydata =  Observable.fromPromise(this.databaseService.isAlreadySubmited('UqpKeNXQHPZrQ03OgSpfpwnQv0G2'));



    this.answer = {
      class_code: "SAMITH123",
      user: this.user.ref,
      profile: {
        firmName: "Aux",
        ans1: 1,
        ans2: 2,
        ans3: 3,
        ans4: 4,
      },
      questionnaire: {
        ans1: 1,
        ans2: 2,
        ans3: 3,
        ans4: 4,
        ans5: 5,
        ans6: 3,
        ans7: 4,
        ans8: 2,
        ans9: 1,
        ans10: 3,

      }
    }

   // this.databaseService.saveAnswer(this.answer);



    //this.countryService.saveCountry(this.country);
    //this.databaseService.saveAnswer(this.answer);
  }
}
