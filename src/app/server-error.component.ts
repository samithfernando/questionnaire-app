import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-server-error',
  template: `
    <div class="modal">
      <div class="modal-background"></div>
      <div class="modal-content">
        xxxxx
      </div>
      <button class="modal-close is-large" aria-label="close"></button>
    </div>
  `,
  styles: [`
    .modal {
      z-index: 999;
    }
  `]
})
export class ServerErrorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
