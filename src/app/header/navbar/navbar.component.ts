import {Component, OnInit, Input, AfterViewInit} from '@angular/core';
import {AuthService} from "../../core/auth.service";
import {Router} from "@angular/router";

declare let $:any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, AfterViewInit {

  loggedUser;

  constructor(
    public auth: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.auth.authState
      .take(1)
      .subscribe(user => this.loggedUser = user);
  }

  logout() {
    this.auth.signOut();
    this.router.navigate(['/login']);
  }

  showQuestionnaire() {
    this.router.navigate(['/user/profile/section1']);
  }

  ngAfterViewInit() {
    // Get all "navbar-burger" elements
    const _navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any navbar burgers
    if (_navbarBurgers.length > 0) {

      // Add a click event on each of them
      _navbarBurgers.forEach(function ($el) {
        $el.addEventListener('click', function () {

          // Get the target from the "data-target" attribute
          const target = $el.dataset.target;
          const _target = document.getElementById(target);

          // Toggle the class on both the "navbar-burger" and the "navbar-menu"
          $el.classList.toggle('is-active');
          _target.classList.toggle('is-active');

        });
      });
    }
  }

}
