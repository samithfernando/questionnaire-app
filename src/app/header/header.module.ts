import { NgModule } from '@angular/core';
import { NavbarComponent } from './navbar/navbar.component';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    RouterModule,
    SharedModule
  ],
  declarations: [NavbarComponent],
  exports: [NavbarComponent]
})
export class HeaderModule { }
