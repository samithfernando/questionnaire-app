export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCyZrB79aJmKumXauxGq0mGI9CJ0_IltaM",
    authDomain: "digitalstrategy-institute.firebaseapp.com",
    databaseURL: "https://digitalstrategy-institute.firebaseio.com",
    projectId: "digitalstrategy-institute",
    storageBucket: "digitalstrategy-institute.appspot.com",
    messagingSenderId: "576807322379"
  }
};
