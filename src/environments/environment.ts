// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyD2GUG-Mbtbn9_ECL8uIhF1nlcA76sJ0eA",
    authDomain: "ds-app-test.firebaseapp.com",
    databaseURL: "https://ds-app-test.firebaseio.com",
    projectId: "ds-app-test",
    storageBucket: "ds-app-test.appspot.com",
    messagingSenderId: "79884141947"
  }
};
